package com.conceptioni.newsapp;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

/**
 * Created by 123 on 21-02-2017.
 */

public class NewsApp extends Application {

    private static NewsApp mInstance;

    public NewsApp() {
        mInstance = this;
    }

    public static Context getContext() {
        return mInstance;
    }

    public static final String TAG = NewsApp.class.getSimpleName();

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

    }

}
