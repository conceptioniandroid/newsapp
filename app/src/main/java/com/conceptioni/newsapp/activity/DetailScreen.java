package com.conceptioni.newsapp.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.conceptioni.newsapp.NewsApp;
import com.conceptioni.newsapp.R;

import java.io.IOException;

public class DetailScreen extends AppCompatActivity {

    String URL;
    String Name;
    WebView webview01;
    ProgressDialog progressDialog;
    ImageView backiv;
    TextView titletv;
    TextView checkinternettvr;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_layout);
        Thread.currentThread().setDefaultUncaughtExceptionHandler(new MyUncaughtExceptionHandler());
        init();
        allclick();
    }

    private void allclick() {
        backiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public static boolean isNetConnectionAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) NewsApp.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    @SuppressLint("SetJavaScriptEnabled")
    private void init() {

        webview01 = findViewById(R.id.webview01);
        backiv = findViewById(R.id.backiv);
        titletv = findViewById(R.id.titletv);
        checkinternettvr = findViewById(R.id.checkinternettvr);

        if (getIntent().getExtras() != null) {
            URL = getIntent().getStringExtra("URL");
            Name = getIntent().getStringExtra("Name");

            titletv.setText(Name);
        }

        if (isNetConnectionAvailable()) {

            checkinternettvr.setVisibility(View.GONE);
            webview01.setVisibility(View.VISIBLE);
            progressDialog = new ProgressDialog(DetailScreen.this);
            progressDialog.setMessage(getResources().getString(R.string.Pleasewait));
            progressDialog.setCancelable(false);

            webview01.setWebViewClient(new myWebClient());
            webview01.getSettings().setJavaScriptEnabled(true);
            webview01.loadUrl(URL);

        } else {
            webview01.setVisibility(View.GONE);
            checkinternettvr.setVisibility(View.VISIBLE);

            checkinternettvr.setText(getResources().getString(R.string.checkinternet));
        }


    }

    public class myWebClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            // TODO Auto-generated method stub
            progressDialog.show();
            super.onPageStarted(view, url, favicon);

        }


        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            // TODO Auto-generated method stub

            //view.loadUrl("http://www.ieta.space/testserver/admin.php");
            return false;

        }

        @Override
        public void onPageFinished(WebView view, String url) {
            // TODO Auto-generated method stub
            super.onPageFinished(view, url);

            progressDialog.dismiss();
        }
    }

    public static class MyUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
            if (ex.getClass().equals(OutOfMemoryError.class)) {
                try {
                    android.os.Debug.dumpHprofData("/sdcard/dump.hprof");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            ex.printStackTrace();
        }
    }
}
