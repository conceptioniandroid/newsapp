package com.conceptioni.newsapp.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.conceptioni.newsapp.NewsApp;
import com.conceptioni.newsapp.R;
import com.conceptioni.newsapp.fragment.FavouriteNewsFragment;
import com.conceptioni.newsapp.fragment.FavouriteSearch;
import com.conceptioni.newsapp.fragment.LatestNewsFragment;
import com.conceptioni.newsapp.fragment.PopularNewsFragment;
import com.conceptioni.newsapp.utils.Constant;
import com.conceptioni.newsapp.utils.SharedPrefs;
import com.conceptioni.newsapp.utils.TextViewRegularBold;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeActivity extends AppCompatActivity {

    FrameLayout content;
    BottomNavigationView bottomnavigation;
    FragmentTransaction ft;
    TextViewRegularBold titletvb;
    List<String> deletedId;
    int gettabpos = 0;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Thread.currentThread().setDefaultUncaughtExceptionHandler(new MyUncaughtExceptionHandler());

//        Configuration configuration = getResources().getConfiguration();
//        configuration.setLayoutDirection(new Locale("fa"));
//        getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());

        init();
    }

    public static boolean isNetConnectionAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) NewsApp.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void init() {
        content = findViewById(R.id.content);
        titletvb = findViewById(R.id.titletvb);
        bottomnavigation = findViewById(R.id.bottomnavigation);
        bottomnavigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);

        BottomNavigationMenuView menuView = (BottomNavigationMenuView) bottomnavigation.getChildAt(0);
        for (int i = 0; i < menuView.getChildCount(); i++) {
            final View iconView = menuView.getChildAt(i).findViewById(android.support.design.R.id.icon);
            final ViewGroup.LayoutParams layoutParams = iconView.getLayoutParams();
            final DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
            layoutParams.height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.botton_icon_size), displayMetrics);
            layoutParams.width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.botton_icon_size), displayMetrics);
            iconView.setLayoutParams(layoutParams);
        }

        if (isNetConnectionAvailable()) {
            CallToken();
        }

        if (Constant.IsFrom.equalsIgnoreCase("Notification")) {
            if (getIntent().getExtras() != null) {
                String NewsID = getIntent().getStringExtra("Id");
                String[] elements = NewsID.split(",");
                deletedId = Arrays.asList(elements);
                Constant.deletedId.addAll(deletedId);
            }
        }

        openfragment();
    }

    private void openfragment() {
        gettabpos = 0;
        Fragment fragment = new LatestNewsFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.content, fragment);
        ft.commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            if (bottomnavigation.getSelectedItemId() != menuItem.getItemId()) {
                switch (menuItem.getItemId()) {
                    case R.id.latest_news:
                        gettabpos = 0;
                        changeFragment(new LatestNewsFragment());
                        return true;

                    case R.id.popular_news:
                        gettabpos = 1;
                        changeFragment(new PopularNewsFragment());
                        return true;

                    case R.id.search_news:
                        gettabpos = 2;
                        changeFragment(new FavouriteSearch());
                        return true;

                    case R.id.favourite_news:
                        gettabpos = 3;
                        changeFragment(new FavouriteNewsFragment());
                        return true;
                }
            }
            return false;
        }
    };

    public void changeFragment(Fragment targetFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        ft = fragmentManager.beginTransaction();
        ft.replace(R.id.content, targetFragment);
        ft.commit();
    }

    public static class MyUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
        @SuppressLint("SdCardPath")
        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
            if (ex.getClass().equals(OutOfMemoryError.class)) {
                try {
                    android.os.Debug.dumpHprofData("/sdcard/dump.hprof");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            ex.printStackTrace();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    private void CallToken() {
        @SuppressLint({"NewApi", "LocalSuppress"}) RequestQueue requestQueue = Volley.newRequestQueue(HomeActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.Api_userNotification, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.optString("success").equalsIgnoreCase("1")) {
                            Log.d("TAG", "onResponse: ");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(HomeActivity.this, "Please Try After Some Time", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("API", "user_notification");
                params.put("var_deviceid", SharedPrefs.getSharedPref().getString(SharedPrefs.tokendetail.refreshtoken, "N/A"));

                Log.d("TAG", "getParams: " + params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(90000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        if (gettabpos == 0) {
            super.onBackPressed();
        } else {
            changeFragment(new LatestNewsFragment());
            gettabpos = 0;
            bottomnavigation.getMenu().getItem(0).setChecked(true);
        }

    }
}
