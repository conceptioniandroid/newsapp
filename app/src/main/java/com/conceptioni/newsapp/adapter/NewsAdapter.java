package com.conceptioni.newsapp.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.conceptioni.newsapp.R;
import com.conceptioni.newsapp.activity.DetailScreen;
import com.conceptioni.newsapp.model.NewsModelDb;
import com.conceptioni.newsapp.utils.TextViewRegular;
import com.conceptioni.newsapp.utils.TextviewSemiBold;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    private Context context;
    private List<NewsModelDb> newsModelArrayList;

    public NewsAdapter(List<NewsModelDb> newsModelArrayList) {
        this.newsModelArrayList = newsModelArrayList;
    }


    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        context = viewGroup.getContext();
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_item_latest_news, viewGroup, false);
        return new NewsViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull final NewsViewHolder newsViewHolder, @SuppressLint("RecyclerView") final int i) {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        newsViewHolder.news_itemll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, DetailScreen.class).putExtra("URL", newsModelArrayList.get(i).getNews_link()).putExtra("Name", newsModelArrayList.get(i).getNews_title()));
            }
        });

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.color.noimage)
                .error(R.color.noimage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .priority(Priority.NORMAL);
        Glide.with(context).load(newsModelArrayList.get(i).getNews_Image()).apply(options).into(newsViewHolder.newsiv);

        RequestOptions options2 = new RequestOptions()
                .centerCrop()
                .placeholder(R.color.noimage)
                .error(R.color.noimage)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .dontAnimate()
                .override(80, 80)
                .priority(Priority.NORMAL);

        Glide.with(context).load(newsModelArrayList.get(i).getLogo()).apply(options2).into(newsViewHolder.web_image);

        newsViewHolder.newstitletvb.setText(newsModelArrayList.get(i).getNews_title());

        newsViewHolder.weblinktvr.setText(newsModelArrayList.get(i).getLink_url());

        Log.d("TAG", "onBindViewHolder: " + newsModelArrayList.get(i).getIsFavourite());

        if (newsModelArrayList.get(i).getIsFavourite().equalsIgnoreCase("true")) {
            newsViewHolder.favouriteiv.setImageResource(R.drawable.fil_favorite);
        } else {
            newsViewHolder.favouriteiv.setImageResource(R.drawable.unfil_favorite);
        }
    }

    @Override
    public int getItemCount() {
        return newsModelArrayList.size();
    }

    class NewsViewHolder extends RecyclerView.ViewHolder {
        LinearLayout news_itemll;
        RoundedImageView newsiv;
        TextviewSemiBold newstitletvb;
        TextViewRegular weblinktvr;
        CircleImageView web_image;
        ImageView favouriteiv;

        NewsViewHolder(@NonNull View itemView) {
            super(itemView);
            news_itemll = itemView.findViewById(R.id.news_itemll);
            newsiv = itemView.findViewById(R.id.newsiv);
            newstitletvb = itemView.findViewById(R.id.newstitletvb);
            weblinktvr = itemView.findViewById(R.id.weblinktvr);
            web_image = itemView.findViewById(R.id.web_image);
            favouriteiv = itemView.findViewById(R.id.favouriteiv);

        }
    }


}
