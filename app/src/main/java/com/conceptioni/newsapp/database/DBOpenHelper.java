package com.conceptioni.newsapp.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.conceptioni.newsapp.model.NewsModelDb;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by pradip on 26-05-2016.
 */
public class DBOpenHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "NewsApp.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TABLE_LATEST_NEWS = "TABLE_LATEST_NEWS";
    public static final String TABLE_LATEST_NEWS_FAVOURITE = "TABLE_LATEST_NEWS_FAVOURITE";
    public static final String TABLE_ALL_NEWS = "TABLE_ALL_NEWS";
    public static final String TABLE_ALL_NEWS_FAVOURITE = "TABLE_ALL_NEWS_FAVOURITE";
    public static final String TABLE_POPULAR_NEWS = "TABLE_POPULAR_NEWS";
    public static final String TABLE_All_POPULAR_NEWS = "TABLE_All_POPULAR_NEWS";

    public static final String COLUMN_NEWS_ID = "COLUMN_NEWS_ID";
    public static final String COLUMN_NEWS_IMAGE = "COLUMN_NEWS_IMAGE";
    public static final String COLUMN_NEWS_TITLE = "COLUMN_NEWS_TITLE";
    public static final String COLUMN_NEWS_ISFAVOURITE = "COLUMN_NEWS_ISFAVOURITE";
    public static final String COLUMN_NEWS_LOGO = "COLUMN_NEWS_LOGO";
    public static final String COLUMN_NEWS_URL = "COLUMN_NEWS_URL";
    public static final String COLUMN_NEWS_DATA = "COLUMN_NEWS_DATA";
    public static final String COLUMN_News_Link_Url = "COLUMN_News_Link_Url";


    public static final String COLUMN_FAVOURITE_NEWS_ID = "COLUMN_NEWS_ID";
    public static final String COLUMN_FAVOURITE_NEWS_IMAGE = "COLUMN_NEWS_IMAGE";
    public static final String COLUMN_FAVOURITE_NEWS_TITLE = "COLUMN_NEWS_TITLE";
    public static final String COLUMN_FAVOURITE_NEWS_ISFAVOURITE = "COLUMN_NEWS_ISFAVOURITE";
    public static final String COLUMN_FAVOURITE_NEWS_LOGO = "COLUMN_NEWS_LOGO";
    public static final String COLUMN_FAVOURITE_NEWS_URL = "COLUMN_NEWS_URL";

    float total;
    Context context;

    private static final String CREATE_TABLE_LATEST_NEWS = "CREATE TABLE " + TABLE_LATEST_NEWS
            + " (" + COLUMN_NEWS_ID + " TEXT, " + COLUMN_NEWS_IMAGE + " TEXT, " + COLUMN_NEWS_TITLE + " TEXT, " + COLUMN_NEWS_ISFAVOURITE + " TEXT, " + COLUMN_NEWS_LOGO + " TEXT, " + COLUMN_News_Link_Url + " TEXT, " + COLUMN_NEWS_URL + " TEXT " + ") ";

    private static final String CREATE_TABLE_All_POPULAR_NEWS = "CREATE TABLE " + TABLE_All_POPULAR_NEWS
            + " (" + COLUMN_NEWS_ID + " TEXT, " + COLUMN_NEWS_IMAGE + " TEXT, " + COLUMN_NEWS_TITLE + " TEXT, " + COLUMN_NEWS_ISFAVOURITE + " TEXT, " + COLUMN_NEWS_LOGO + " TEXT, " + COLUMN_News_Link_Url + " TEXT, " + COLUMN_NEWS_URL + " TEXT " + ") ";


    private static final String CREATE_TABLE_LATEST_NEWS_FAVOURITE = "CREATE TABLE " + TABLE_LATEST_NEWS_FAVOURITE
            + " (" + COLUMN_NEWS_ID + " TEXT, " + COLUMN_NEWS_IMAGE + " TEXT, " + COLUMN_NEWS_TITLE + " TEXT, " + COLUMN_NEWS_ISFAVOURITE + " TEXT, " + COLUMN_NEWS_LOGO + " TEXT, " + COLUMN_News_Link_Url + " TEXT, " +  COLUMN_NEWS_URL + " TEXT " + ") ";


    private static final String CREATE_TABLE_ALL_NEWS = "CREATE TABLE " + TABLE_ALL_NEWS
            + " (" + COLUMN_NEWS_DATA + " TEXT " + ") ";

    private static final String CREATE_TABLE_POPULAR_NEWS = "CREATE TABLE " + TABLE_POPULAR_NEWS
            + " (" + COLUMN_NEWS_DATA + " TEXT " + ") ";

    private static final String CREATE_TABLE_ALL_NEWS_FAVOURITE = "CREATE TABLE " + TABLE_ALL_NEWS_FAVOURITE
            + " (" + COLUMN_NEWS_DATA + " TEXT " + ") ";


    private ArrayList<NewsModelDb> favouriteModelDbArrayList = new ArrayList<>();
    private ArrayList<NewsModelDb> newsModelDbArrayList = new ArrayList<>();
    private ArrayList<NewsModelDb> newsModelIdArrayList = new ArrayList<>();
    private ArrayList<NewsModelDb> popularnewsModelDbArrayList = new ArrayList<>();
    private ArrayList<NewsModelDb> popularnewsModelidArrayList = new ArrayList<>();


    public DBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_LATEST_NEWS);
        db.execSQL(CREATE_TABLE_LATEST_NEWS_FAVOURITE);
        db.execSQL(CREATE_TABLE_ALL_NEWS);
        db.execSQL(CREATE_TABLE_ALL_NEWS_FAVOURITE);
        db.execSQL(CREATE_TABLE_POPULAR_NEWS);
        db.execSQL(CREATE_TABLE_All_POPULAR_NEWS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void addfavouritenews(String newsid, String title, String image, String logo, String url, String isfavourite,String link_url) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NEWS_ID, newsid);
        contentValues.put(COLUMN_NEWS_TITLE, title);
        contentValues.put(COLUMN_NEWS_IMAGE, image);
        contentValues.put(COLUMN_NEWS_LOGO, logo);
        contentValues.put(COLUMN_NEWS_URL, url);
        contentValues.put(COLUMN_NEWS_ISFAVOURITE, isfavourite);
        contentValues.put(COLUMN_News_Link_Url, link_url);
        db.insert(TABLE_LATEST_NEWS_FAVOURITE, null, contentValues);
        Log.e("++++++favourite", "++++" + contentValues);
    }

    public void addNewsData(String newsid, String title, String image, String logo, String url, String isfavourite,String link_url) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NEWS_ID, newsid);
        contentValues.put(COLUMN_NEWS_TITLE, title);
        contentValues.put(COLUMN_NEWS_IMAGE, image);
        contentValues.put(COLUMN_NEWS_LOGO, logo);
        contentValues.put(COLUMN_NEWS_URL, url);
        contentValues.put(COLUMN_NEWS_ISFAVOURITE, isfavourite);
        contentValues.put(COLUMN_News_Link_Url, link_url);
        Log.d("TAG", "addNewsData: " + contentValues);

        db.insert(TABLE_LATEST_NEWS, null, contentValues);
    }

    public void Addpopularnews(String newsid, String title, String image, String logo, String url, String isfavourite,String link_url) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NEWS_ID, newsid);
        contentValues.put(COLUMN_NEWS_TITLE, title);
        contentValues.put(COLUMN_NEWS_IMAGE, image);
        contentValues.put(COLUMN_NEWS_LOGO, logo);
        contentValues.put(COLUMN_NEWS_URL, url);
        contentValues.put(COLUMN_NEWS_ISFAVOURITE, isfavourite);
        contentValues.put(COLUMN_News_Link_Url, link_url);
        Log.d("TAG", "addNewsData: " + contentValues);

        db.insert(TABLE_All_POPULAR_NEWS, null, contentValues);
    }

    public void updatenewsdata(String newsid, String title, String image, String logo, String url, String isfavourite,String link_url) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NEWS_ID, newsid);
        contentValues.put(COLUMN_NEWS_TITLE, title);
        contentValues.put(COLUMN_NEWS_IMAGE, image);
        contentValues.put(COLUMN_NEWS_LOGO, logo);
        contentValues.put(COLUMN_NEWS_URL, url);
        contentValues.put(COLUMN_NEWS_ISFAVOURITE, isfavourite);
        contentValues.put(COLUMN_News_Link_Url, link_url);
        database.update(TABLE_LATEST_NEWS, contentValues, COLUMN_NEWS_ID + " = " + newsid, null);

    }

    public void updatepopularnewsdata(String newsid, String title, String image, String logo, String url, String isfavourite,String link_url) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_NEWS_ID, newsid);
        contentValues.put(COLUMN_NEWS_TITLE, title);
        contentValues.put(COLUMN_NEWS_IMAGE, image);
        contentValues.put(COLUMN_NEWS_LOGO, logo);
        contentValues.put(COLUMN_NEWS_URL, url);
        contentValues.put(COLUMN_NEWS_ISFAVOURITE, isfavourite);
        contentValues.put(COLUMN_News_Link_Url, link_url);
        database.update(TABLE_All_POPULAR_NEWS, contentValues, COLUMN_NEWS_ID + " = " + newsid, null);

    }

    public List<NewsModelDb> getallNews() {
        newsModelDbArrayList.clear();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_LATEST_NEWS, null);
        Log.e("++++id", "+++" + res.getCount());
        if (res.getCount() > 0) {
            res.moveToFirst();
            for (int i = 0; i < res.getCount(); i++) {
                NewsModelDb newsModelDb = new NewsModelDb();
                newsModelDb.setNews_Id(res.getString(0));
                newsModelDb.setNews_Image(res.getString(1));
                newsModelDb.setNews_title(res.getString(2));
                newsModelDb.setIsFavourite(res.getString(3));
                newsModelDb.setLogo(res.getString(4));
                newsModelDb.setNews_link(res.getString(5));
                newsModelDb.setLink_url(res.getString(6));
                newsModelDbArrayList.add(newsModelDb);
                res.moveToNext();
            }
        }
        return newsModelDbArrayList;
    }

    public List<NewsModelDb> getallpopularNews() {
        popularnewsModelDbArrayList.clear();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_All_POPULAR_NEWS, null);
        Log.e("++++id", "+++" + res.getCount());
        if (res.getCount() > 0) {
            res.moveToFirst();
            for (int i = 0; i < res.getCount(); i++) {
                NewsModelDb newsModelDb = new NewsModelDb();
                newsModelDb.setNews_Id(res.getString(0));
                newsModelDb.setNews_Image(res.getString(1));
                newsModelDb.setNews_title(res.getString(2));
                newsModelDb.setIsFavourite(res.getString(3));
                newsModelDb.setLogo(res.getString(4));
                newsModelDb.setNews_link(res.getString(5));
                newsModelDb.setLink_url(res.getString(6));
                popularnewsModelDbArrayList.add(newsModelDb);
                res.moveToNext();
            }
        }
        return popularnewsModelDbArrayList;
    }

    public List<NewsModelDb> getnews(String NewsId) {
        newsModelIdArrayList.clear();
        SQLiteDatabase db = this.getReadableDatabase();
        @SuppressLint("Recycle") Cursor res = db.rawQuery("select * from " + TABLE_LATEST_NEWS + " WHERE " + COLUMN_NEWS_ID + " = " + NewsId, null);
        if (res.getCount() > 0) {
            res.moveToFirst();
            for (int i = 0; i < res.getCount(); i++) {
                NewsModelDb newsModelDb = new NewsModelDb();
                newsModelDb.setNews_Id(res.getString(0));
                newsModelDb.setNews_Image(res.getString(1));
                newsModelDb.setNews_title(res.getString(2));
                newsModelDb.setIsFavourite(res.getString(3));
                newsModelDb.setLogo(res.getString(4));
                newsModelDb.setNews_link(res.getString(5));
                newsModelDb.setLink_url(res.getString(6));
                newsModelIdArrayList.add(newsModelDb);
                res.moveToNext();
            }
        }
        return newsModelIdArrayList;
    }

    public List<NewsModelDb> getpopularidnews(String NewsId) {
        popularnewsModelidArrayList.clear();
        SQLiteDatabase db = this.getReadableDatabase();
        @SuppressLint("Recycle") Cursor res = db.rawQuery("select * from " + TABLE_All_POPULAR_NEWS + " WHERE " + COLUMN_NEWS_ID + " = " + NewsId, null);
        if (res.getCount() > 0) {
            res.moveToFirst();
            for (int i = 0; i < res.getCount(); i++) {
                NewsModelDb newsModelDb = new NewsModelDb();
                newsModelDb.setNews_Id(res.getString(0));
                newsModelDb.setNews_Image(res.getString(1));
                newsModelDb.setNews_title(res.getString(2));
                newsModelDb.setIsFavourite(res.getString(3));
                newsModelDb.setLogo(res.getString(4));
                newsModelDb.setNews_link(res.getString(5));
                newsModelDb.setLink_url(res.getString(6));
                popularnewsModelidArrayList.add(newsModelDb);
                res.moveToNext();
            }
        }
        return popularnewsModelidArrayList;
    }

    public ArrayList getFavouriteNews() {
        favouriteModelDbArrayList.clear();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_LATEST_NEWS_FAVOURITE, null);
        Log.e("++++id", "+++" + res.getCount());
        if (res.getCount() > 0) {
            res.moveToFirst();
            for (int i = 0; i < res.getCount(); i++) {
                NewsModelDb newsModelDb = new NewsModelDb();
                newsModelDb.setNews_Id(res.getString(0));
                newsModelDb.setNews_Image(res.getString(1));
                newsModelDb.setNews_title(res.getString(2));
                newsModelDb.setIsFavourite(res.getString(3));
                newsModelDb.setLogo(res.getString(4));
                newsModelDb.setNews_link(res.getString(5));
                newsModelDb.setLink_url(res.getString(6));
                favouriteModelDbArrayList.add(newsModelDb);
                res.moveToNext();
            }
        }
        return favouriteModelDbArrayList;
    }


    public void deteleNewstable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_LATEST_NEWS);
    }

    public void deletepopularenews() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + TABLE_All_POPULAR_NEWS);
    }

    public Integer deleteFavouriteNewsData(String news_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_LATEST_NEWS_FAVOURITE, COLUMN_NEWS_ID + " = " + news_id, null);
    }

}
