package com.conceptioni.newsapp.firebase;

import android.util.Log;

import com.conceptioni.newsapp.utils.SharedPrefs;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {


    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("++++++token", "++++" + refreshedToken);
        SharedPrefs.getSharedPref().edit().putString(SharedPrefs.tokendetail.refreshtoken, refreshedToken).apply();
        Log.d("+++++token", "++++" + SharedPrefs.getSharedPref().getString(SharedPrefs.tokendetail.refreshtoken, "N/A"));

    }


}