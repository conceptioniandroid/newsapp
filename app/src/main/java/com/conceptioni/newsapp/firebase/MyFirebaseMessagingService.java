package com.conceptioni.newsapp.firebase;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.conceptioni.newsapp.R;
import com.conceptioni.newsapp.activity.HomeActivity;
import com.conceptioni.newsapp.utils.Constant;
import com.conceptioni.newsapp.utils.SharedPrefs;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    String NewsId = "";

    @SuppressLint("NewApi")
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d("++++++data", "+++++" + remoteMessage.getData() + "+++");

        if (remoteMessage.getData() != null) {
            JSONObject data = new JSONObject(remoteMessage.getData());
            String type = data.optString("type");
            NewsId = data.optString("id");
            SharedPrefs.getSharedPref().edit().putString(SharedPrefs.userdetail.News_Id, NewsId).apply();
            if (type.equalsIgnoreCase("add_news")) {
                sendnotification();
            } else if (type.equalsIgnoreCase("update_news")) {
                Log.d("TAG", "onMessageReceived: ");
            } else if (type.equalsIgnoreCase("delete_news")) {
                Log.d("TAG", "onMessageReceived:delete_news " + NewsId);
                if (Constant.active) {
                    updateChatScreen();
                }
            }
        }

    }

    private void sendnotification() {

        Random random = new Random();
        int rand = random.nextInt(1000);
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(MyFirebaseMessagingService.this);
        builder.setContentTitle(getResources().getString(R.string.app_name));
        builder.setContentText("new message received");
        builder.setAutoCancel(true);
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setSound(uri);
        builder.setOnlyAlertOnce(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder.setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_launcher));
            builder.setColor(this.getResources().getColor(R.color.noimage));
        }
        Constant.IsFrom = "Notification";
        Intent resultIntent = new Intent(this, HomeActivity.class);
        resultIntent.putExtra("Id", NewsId);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify((int) System.currentTimeMillis(), builder.build());
    }

    private void updateChatScreen() {
        Intent intent = new Intent("broadcast_chat_message");
        this.sendBroadcast(intent);
    }
}