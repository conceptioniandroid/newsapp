package com.conceptioni.newsapp.fragment;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.conceptioni.newsapp.R;
import com.conceptioni.newsapp.adapter.FavouriteNewsAdapter;
import com.conceptioni.newsapp.database.DBOpenHelper;
import com.conceptioni.newsapp.model.NewsModelDb;
import com.conceptioni.newsapp.utils.RecyclerTouchListener;
import com.conceptioni.newsapp.utils.TextViewRegularBold;

import java.util.ArrayList;
import java.util.List;

public class FavouriteNewsFragment extends Fragment {

    View view;
    RecyclerView home_recycler;
    ArrayList<NewsModelDb> newsModelFavouriteArrayList = new ArrayList<>();
    DBOpenHelper dbOpenHelper;
    SQLiteDatabase sqLiteDatabase;
    TextViewRegularBold nodatatvb;
    FavouriteNewsAdapter newsAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.favourite_news_fragment, container, false);
        initview();
        return view;
    }


    private void initview() {
        home_recycler = view.findViewById(R.id.home_recycler);
        nodatatvb = view.findViewById(R.id.nodatatvb);

        dbOpenHelper = new DBOpenHelper(getActivity());
        sqLiteDatabase = dbOpenHelper.getWritableDatabase();
        sqLiteDatabase = dbOpenHelper.getReadableDatabase();

        newsModelFavouriteArrayList = dbOpenHelper.getFavouriteNews();
        Log.d("TAG", "initview: " + newsModelFavouriteArrayList.size());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        home_recycler.setLayoutManager(layoutManager);

        if (!newsModelFavouriteArrayList.isEmpty()) {
            nodatatvb.setVisibility(View.GONE);
            home_recycler.setVisibility(View.VISIBLE);
            newsAdapter = new FavouriteNewsAdapter(newsModelFavouriteArrayList);
            home_recycler.setAdapter(newsAdapter);
        } else {
            home_recycler.setVisibility(View.GONE);
            nodatatvb.setVisibility(View.VISIBLE);
        }

        home_recycler.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), home_recycler, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                final ImageView favouriteiv = view.findViewById(R.id.favouriteiv);
                final ImageView shareiv = view.findViewById(R.id.shareiv);
                favouriteiv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        List<NewsModelDb> newsModelDbList = new ArrayList<>();
                        List<NewsModelDb> popularnewsModelDbList = new ArrayList<>();
                        newsModelDbList = dbOpenHelper.getnews(newsModelFavouriteArrayList.get(position).getNews_Id());
                        popularnewsModelDbList = dbOpenHelper.getpopularidnews(newsModelFavouriteArrayList.get(position).getNews_Id());
                        Log.d("TAG", "onClick: " + newsModelDbList.size());
                        if (newsModelFavouriteArrayList.get(position).getIsFavourite().equalsIgnoreCase("true")) {
                            favouriteiv.setImageResource(R.drawable.unfil_favorite);
                            newsModelFavouriteArrayList.get(position).setIsFavourite("false");
                            if (!newsModelDbList.isEmpty()) {
                                newsModelDbList.get(0).setIsFavourite("false");
                                dbOpenHelper.updatenewsdata(newsModelDbList.get(0).getNews_Id(), newsModelDbList.get(0).getNews_title(), newsModelDbList.get(0).getNews_Image(), newsModelDbList.get(0).getLogo(), newsModelDbList.get(0).getNews_link(), newsModelDbList.get(0).getIsFavourite(),newsModelDbList.get(0).getLink_url());
                            }
                            if (!popularnewsModelDbList.isEmpty()) {
                                popularnewsModelDbList.get(0).setIsFavourite("false");
                                dbOpenHelper.updatepopularnewsdata(popularnewsModelDbList.get(0).getNews_Id(), popularnewsModelDbList.get(0).getNews_title(), popularnewsModelDbList.get(0).getNews_Image(), popularnewsModelDbList.get(0).getLogo(), popularnewsModelDbList.get(0).getNews_link(), popularnewsModelDbList.get(0).getIsFavourite(),popularnewsModelDbList.get(0).getLink_url());
                            }
                            dbOpenHelper.deleteFavouriteNewsData(newsModelFavouriteArrayList.get(position).getNews_Id());

                            newsModelFavouriteArrayList.remove(position);
                            newsAdapter.notifyDataSetChanged();

                            if (newsModelFavouriteArrayList.isEmpty()) {
                                home_recycler.setVisibility(View.GONE);
                                nodatatvb.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                });

                shareiv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                        String sAux = "\nLet me recommend you this News\n\n";
//                        sAux = sAux + "https://play.google.com/store/movies/details/Jurassic_World_Fallen_Kingdom?id=OexCdfICdGU \n\n";
                        sAux = sAux + newsModelFavouriteArrayList.get(position).getNews_link();
                        i.putExtra(Intent.EXTRA_TEXT, sAux);
                        startActivity(Intent.createChooser(i, "choose one"));
                    }
                });
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }


}
