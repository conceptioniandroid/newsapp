package com.conceptioni.newsapp.fragment;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.conceptioni.newsapp.R;
import com.conceptioni.newsapp.adapter.SearchNewsAdapter;
import com.conceptioni.newsapp.database.DBOpenHelper;
import com.conceptioni.newsapp.model.NewsModelDb;
import com.conceptioni.newsapp.utils.RecyclerTouchListener;
import com.conceptioni.newsapp.utils.TextViewRegularBold;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class FavouriteSearch extends Fragment {

    View view;
    Context context;
    RecyclerView search_recycler;
    SearchView searchView1;
    DBOpenHelper dbOpenHelper;
    SQLiteDatabase sqLiteDatabase;
    Gson gson;
    List<NewsModelDb> newsModelFacouriteDbArrayList1 = new ArrayList<>();
    List<NewsModelDb> newsModelFacouritetempDbArrayList1 = new ArrayList<>();
    SearchNewsAdapter searchNewsAdapter;
    TextViewRegularBold nodatatvb;
    private String mLanguageCode = "ar";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.search_layout, container, false);
        assert container != null;
        context = container.getContext();
        initview();

        return view;
    }

    private void allclick() {
        searchView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchView1.setIconified(false);
            }
        });

        searchView1.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                newsModelFacouriteDbArrayList1.clear();
                search_recycler.setVisibility(View.VISIBLE);
                nodatatvb.setVisibility(View.GONE);
                if (s.equalsIgnoreCase("")) {
                    search_recycler.setVisibility(View.GONE);
                    nodatatvb.setVisibility(View.VISIBLE);
                    newsModelFacouriteDbArrayList1.addAll(newsModelFacouritetempDbArrayList1);
                    searchNewsAdapter.notifyDataSetChanged();
                } else {
                    search_recycler.setVisibility(View.VISIBLE);
                    nodatatvb.setVisibility(View.GONE);

                    for (int i = 0; i < newsModelFacouritetempDbArrayList1.size(); i++) {
                        if (newsModelFacouritetempDbArrayList1.get(i).getNews_title().toUpperCase().contains(s.toUpperCase())) {

                            NewsModelDb newsModelDb = new NewsModelDb();
                            newsModelDb.setNews_Id(newsModelFacouritetempDbArrayList1.get(i).getNews_Id());
                            newsModelDb.setNews_title(newsModelFacouritetempDbArrayList1.get(i).getNews_title());
                            newsModelDb.setNews_Image(newsModelFacouritetempDbArrayList1.get(i).getNews_Image());
                            newsModelDb.setIsFavourite(newsModelFacouritetempDbArrayList1.get(i).getIsFavourite());
                            newsModelDb.setLogo(newsModelFacouritetempDbArrayList1.get(i).getLogo());
                            newsModelDb.setNews_link(newsModelFacouritetempDbArrayList1.get(i).getNews_link());
                            newsModelDb.setLink_url(newsModelFacouritetempDbArrayList1.get(i).getLink_url());
                            newsModelFacouriteDbArrayList1.add(newsModelDb);
                        }
                    }
                    searchNewsAdapter.notifyDataSetChanged();
                }
                return false;
            }
        });

    }

    private void initview() {

        search_recycler = view.findViewById(R.id.search_recycler);
        searchView1 = view.findViewById(R.id.searchView1);
        nodatatvb = view.findViewById(R.id.nodatatvb);

        dbOpenHelper = new DBOpenHelper(getActivity());
        sqLiteDatabase = dbOpenHelper.getWritableDatabase();
        sqLiteDatabase = dbOpenHelper.getReadableDatabase();

        search_recycler.setVisibility(View.GONE);
        nodatatvb.setVisibility(View.VISIBLE);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        search_recycler.setLayoutManager(layoutManager);

        newsModelFacouriteDbArrayList1.clear();
        newsModelFacouriteDbArrayList1 = dbOpenHelper.getallNews();
        newsModelFacouritetempDbArrayList1.addAll(newsModelFacouriteDbArrayList1);
        searchNewsAdapter = new SearchNewsAdapter(newsModelFacouriteDbArrayList1);
        search_recycler.setAdapter(searchNewsAdapter);


        search_recycler.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), search_recycler, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                ImageView shareiv = view.findViewById(R.id.shareiv);
                final ImageView favouriteiv = view.findViewById(R.id.favouriteiv);
                shareiv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                        String sAux = "\nLet me recommend you this News\n\n";
//                        sAux = sAux + "https://play.google.com/store/movies/details/Jurassic_World_Fallen_Kingdom?id=OexCdfICdGU \n\n";
                        sAux = sAux + newsModelFacouriteDbArrayList1.get(position).getNews_link();
                        i.putExtra(Intent.EXTRA_TEXT, sAux);
                        startActivity(Intent.createChooser(i, "choose one"));
                    }
                });

                favouriteiv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        List<NewsModelDb> newsModelDbList = new ArrayList<>();
                        List<NewsModelDb> popularnewsModelDbList = new ArrayList<>();
                        newsModelDbList = dbOpenHelper.getnews(newsModelFacouriteDbArrayList1.get(position).getNews_Id());
                        popularnewsModelDbList = dbOpenHelper.getpopularidnews(newsModelFacouriteDbArrayList1.get(position).getNews_Id());
                        if (newsModelDbList.get(0).getIsFavourite().equalsIgnoreCase("true")) {
                            if (!newsModelDbList.isEmpty()) {
                                newsModelDbList.get(0).setIsFavourite("false");
                                dbOpenHelper.updatenewsdata(newsModelDbList.get(0).getNews_Id(), newsModelDbList.get(0).getNews_title(), newsModelDbList.get(0).getNews_Image(), newsModelDbList.get(0).getLogo(), newsModelDbList.get(0).getNews_link(), newsModelDbList.get(0).getIsFavourite(),newsModelDbList.get(0).getLink_url());
                            }
                            if (!popularnewsModelDbList.isEmpty()) {
                                popularnewsModelDbList.get(0).setIsFavourite("false");
                                dbOpenHelper.updatepopularnewsdata(popularnewsModelDbList.get(0).getNews_Id(), popularnewsModelDbList.get(0).getNews_title(), popularnewsModelDbList.get(0).getNews_Image(), popularnewsModelDbList.get(0).getLogo(), popularnewsModelDbList.get(0).getNews_link(), popularnewsModelDbList.get(0).getIsFavourite(),popularnewsModelDbList.get(0).getLink_url());
                            }
                            favouriteiv.setImageResource(R.drawable.unfil_favorite);
                            newsModelDbList.get(0).setIsFavourite("false");
                            int deleterow = dbOpenHelper.deleteFavouriteNewsData(newsModelDbList.get(0).getNews_Id());
                            if (deleterow > 0) {
                                dbOpenHelper.updatenewsdata(newsModelDbList.get(0).getNews_Id(), newsModelDbList.get(0).getNews_title(), newsModelDbList.get(0).getNews_Image(), newsModelDbList.get(0).getLogo(), newsModelDbList.get(0).getNews_link(), newsModelDbList.get(0).getIsFavourite(),newsModelDbList.get(0).getLink_url());
                            }
                        } else {
                            favouriteiv.setImageResource(R.drawable.fil_favorite);
//                                newsModelDbList.get(position).setIsFavourite("true");

                            if (!newsModelDbList.isEmpty()) {
                                newsModelDbList.get(0).setIsFavourite("true");
                                dbOpenHelper.updatenewsdata(newsModelDbList.get(0).getNews_Id(), newsModelDbList.get(0).getNews_title(), newsModelDbList.get(0).getNews_Image(), newsModelDbList.get(0).getLogo(), newsModelDbList.get(0).getNews_link(), newsModelDbList.get(0).getIsFavourite(),newsModelDbList.get(0).getLink_url());
                            }
                            if (!popularnewsModelDbList.isEmpty()) {
                                popularnewsModelDbList.get(0).setIsFavourite("true");
                                dbOpenHelper.updatepopularnewsdata(popularnewsModelDbList.get(0).getNews_Id(), popularnewsModelDbList.get(0).getNews_title(), popularnewsModelDbList.get(0).getNews_Image(), popularnewsModelDbList.get(0).getLogo(), popularnewsModelDbList.get(0).getNews_link(), popularnewsModelDbList.get(0).getIsFavourite(),popularnewsModelDbList.get(0).getLink_url());
                            }

                            dbOpenHelper.addfavouritenews(newsModelDbList.get(0).getNews_Id(), newsModelDbList.get(0).getNews_title(), newsModelDbList.get(0).getNews_Image(), newsModelDbList.get(0).getLogo(), newsModelDbList.get(0).getNews_link(), newsModelDbList.get(0).getIsFavourite(),newsModelDbList.get(0).getLink_url());
//                                dbOpenHelper.updatenewsdata(newsModelDbList.get(position).getNews_Id(),newsModelDbList.get(position).getNews_title(),newsModelDbList.get(position).getNews_Image(),newsModelDbList.get(position).getLogo(),newsModelDbList.get(position).getNews_link(),newsModelDbList.get(position).getIsFavourite());
                        }
                    }
                });

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        allclick();
    }
}
