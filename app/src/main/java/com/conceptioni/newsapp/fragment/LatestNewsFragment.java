package com.conceptioni.newsapp.fragment;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TashieLoader;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.conceptioni.newsapp.NewsApp;
import com.conceptioni.newsapp.R;
import com.conceptioni.newsapp.adapter.NewsAdapter;
import com.conceptioni.newsapp.database.DBOpenHelper;
import com.conceptioni.newsapp.model.NewsModelDb;
import com.conceptioni.newsapp.utils.Constant;
import com.conceptioni.newsapp.utils.RecyclerTouchListener;
import com.conceptioni.newsapp.utils.SharedPrefs;
import com.conceptioni.newsapp.utils.TextViewRegularBold;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class LatestNewsFragment extends Fragment {

    View view;
    RecyclerView home_recycler;
    TashieLoader tashiloader;
    ArrayList<NewsModelDb> newsModelDbArrayList = new ArrayList<>();
    List<NewsModelDb> newsModelFacouriteDbArrayList1 = new ArrayList<>();
    ArrayList<NewsModelDb> newsModelFavouriteArrayList = new ArrayList<>();
    TextViewRegularBold nodatatvb;
    DBOpenHelper dbOpenHelper;
    SQLiteDatabase sqLiteDatabase;
    Gson gson;
    List<String> deletedId = new ArrayList<>();
    boolean b = true;
    Context context;

    public static boolean isNetConnectionAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) NewsApp.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private BroadcastReceiver messageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("TAG", "onReceive: ");
            CallNews();
        }
    };

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.latest_news_fragment, container, false);
        assert container != null;
        context = container.getContext();
        Thread.currentThread().setDefaultUncaughtExceptionHandler(new MyUncaughtExceptionHandler());

        initview();

        if (b) {
            Log.d("TAG", "onCreateView: ");
            Constant.active = true;
        }
        return view;
    }

    private void initview() {
        home_recycler = view.findViewById(R.id.home_recycler);
        tashiloader = view.findViewById(R.id.tashiloader);
        nodatatvb = view.findViewById(R.id.nodatatvb);

        dbOpenHelper = new DBOpenHelper(getActivity());
        sqLiteDatabase = dbOpenHelper.getWritableDatabase();
        sqLiteDatabase = dbOpenHelper.getReadableDatabase();

        gson = new Gson();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        home_recycler.setLayoutManager(layoutManager);


        if (isNetConnectionAvailable()) {
            CallNews();
        } else {
//            List<NewsModelDb> newsModelDbs = new ArrayList<>();
//            newsModelDbs = dbOpenHelper.getallNews();
            newsModelFacouriteDbArrayList1.clear();
            newsModelFacouriteDbArrayList1 = dbOpenHelper.getallNews();
            Log.d("TAG", "initview: " + newsModelFacouriteDbArrayList1.size());
            if (!newsModelFacouriteDbArrayList1.isEmpty()) {
                nodatatvb.setVisibility(View.GONE);
                NewsAdapter newsAdapter = new NewsAdapter(newsModelFacouriteDbArrayList1);
                home_recycler.setAdapter(newsAdapter);
            } else {
                nodatatvb.setVisibility(View.VISIBLE);
                nodatatvb.setText("No News Found");
            }

//            String NewsData = dbOpenHelper.getAllNews();
//            if (!NewsData.equalsIgnoreCase("")) {
//                newsModelFacouriteDbArrayList1.clear();
//                newsModelFacouriteDbArrayList1 = gson.fromJson(NewsData, new TypeToken<List<NewsModelDb>>() {
//                }.getType());
//                NewsAdapter newsAdapter = new NewsAdapter(newsModelFacouriteDbArrayList1);
//                home_recycler.setAdapter(newsAdapter);
//            }
        }

        home_recycler.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), home_recycler, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                final ImageView favouriteiv = view.findViewById(R.id.favouriteiv);
                final ImageView shareiv = view.findViewById(R.id.shareiv);
                favouriteiv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isNetConnectionAvailable()) {
                            if (newsModelDbArrayList.get(position).getIsFavourite().equalsIgnoreCase("true")) {
                                favouriteiv.setImageResource(R.drawable.unfil_favorite);
                                newsModelDbArrayList.get(position).setIsFavourite("false");
                                dbOpenHelper.deleteFavouriteNewsData(newsModelDbArrayList.get(position).getNews_Id());
                            } else {
                                favouriteiv.setImageResource(R.drawable.fil_favorite);
                                newsModelDbArrayList.get(position).setIsFavourite("true");
                                dbOpenHelper.addfavouritenews(newsModelDbArrayList.get(position).getNews_Id(), newsModelDbArrayList.get(position).getNews_title(), newsModelDbArrayList.get(position).getNews_Image(), newsModelDbArrayList.get(position).getLogo(), newsModelDbArrayList.get(position).getNews_link(), newsModelDbArrayList.get(position).getIsFavourite(),newsModelDbArrayList.get(position).getLink_url());
                            }
                        } else {
                            List<NewsModelDb> newsModelDbList = new ArrayList<>();
                            List<NewsModelDb> popularnewsModelDbList = new ArrayList<>();
                            newsModelDbList = dbOpenHelper.getallNews();
                            popularnewsModelDbList = dbOpenHelper.getpopularidnews(newsModelFacouriteDbArrayList1.get(position).getNews_Id());
                            if (newsModelDbList.get(position).getIsFavourite().equalsIgnoreCase("true")) {
                                if (!newsModelDbList.isEmpty()) {
                                    newsModelDbList.get(0).setIsFavourite("false");
                                    dbOpenHelper.updatenewsdata(newsModelDbList.get(0).getNews_Id(), newsModelDbList.get(0).getNews_title(), newsModelDbList.get(0).getNews_Image(), newsModelDbList.get(0).getLogo(), newsModelDbList.get(0).getNews_link(), newsModelDbList.get(0).getIsFavourite(),newsModelDbList.get(0).getLink_url());
                                }
                                if (!popularnewsModelDbList.isEmpty()) {
                                    popularnewsModelDbList.get(0).setIsFavourite("false");
                                    dbOpenHelper.updatepopularnewsdata(popularnewsModelDbList.get(0).getNews_Id(), popularnewsModelDbList.get(0).getNews_title(), popularnewsModelDbList.get(0).getNews_Image(), popularnewsModelDbList.get(0).getLogo(), popularnewsModelDbList.get(0).getNews_link(), popularnewsModelDbList.get(0).getIsFavourite(),popularnewsModelDbList.get(0).getLink_url());
                                }
                                favouriteiv.setImageResource(R.drawable.unfil_favorite);
                                newsModelDbList.get(position).setIsFavourite("false");
                                int deleterow = dbOpenHelper.deleteFavouriteNewsData(newsModelDbList.get(position).getNews_Id());
                                if (deleterow > 0) {
                                    dbOpenHelper.updatenewsdata(newsModelDbList.get(position).getNews_Id(), newsModelDbList.get(position).getNews_title(), newsModelDbList.get(position).getNews_Image(), newsModelDbList.get(position).getLogo(), newsModelDbList.get(position).getNews_link(), newsModelDbList.get(position).getIsFavourite(),newsModelDbList.get(position).getLink_url());
                                }
                            } else {
                                favouriteiv.setImageResource(R.drawable.fil_favorite);
//                                newsModelDbList.get(position).setIsFavourite("true");

                                if (!newsModelDbList.isEmpty()) {
                                    newsModelDbList.get(position).setIsFavourite("true");
                                    dbOpenHelper.updatenewsdata(newsModelDbList.get(position).getNews_Id(), newsModelDbList.get(position).getNews_title(), newsModelDbList.get(position).getNews_Image(), newsModelDbList.get(position).getLogo(), newsModelDbList.get(position).getNews_link(), newsModelDbList.get(position).getIsFavourite(),newsModelDbList.get(position).getLink_url());
                                }
                                if (!popularnewsModelDbList.isEmpty()) {
                                    popularnewsModelDbList.get(0).setIsFavourite("true");
                                    dbOpenHelper.updatepopularnewsdata(popularnewsModelDbList.get(0).getNews_Id(), popularnewsModelDbList.get(0).getNews_title(), popularnewsModelDbList.get(0).getNews_Image(), popularnewsModelDbList.get(0).getLogo(), popularnewsModelDbList.get(0).getNews_link(), popularnewsModelDbList.get(0).getIsFavourite(),popularnewsModelDbList.get(0).getLink_url());
                                }

                                dbOpenHelper.addfavouritenews(newsModelDbList.get(position).getNews_Id(), newsModelDbList.get(position).getNews_title(), newsModelDbList.get(position).getNews_Image(), newsModelDbList.get(position).getLogo(), newsModelDbList.get(position).getNews_link(), newsModelDbList.get(position).getIsFavourite(),newsModelDbList.get(position).getLink_url());
//                                dbOpenHelper.updatenewsdata(newsModelDbList.get(position).getNews_Id(),newsModelDbList.get(position).getNews_title(),newsModelDbList.get(position).getNews_Image(),newsModelDbList.get(position).getLogo(),newsModelDbList.get(position).getNews_link(),newsModelDbList.get(position).getIsFavourite());
                            }

                        }
                    }
                });

                shareiv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                        String sAux = "\nLet me recommend you this News\n\n";
                        if (isNetConnectionAvailable()) {
//                        sAux = sAux + "https://play.google.com/store/movies/details/Jurassic_World_Fallen_Kingdom?id=OexCdfICdGU \n\n";
                            sAux = sAux + newsModelDbArrayList.get(position).getNews_link();
                            i.putExtra(Intent.EXTRA_TEXT, sAux);
                            startActivity(Intent.createChooser(i, "choose one"));
                        } else {
                            sAux = sAux + newsModelFacouriteDbArrayList1.get(position).getNews_link();
                            i.putExtra(Intent.EXTRA_TEXT, sAux);
                            startActivity(Intent.createChooser(i, "choose one"));
                        }
                    }
                });
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void CallNews() {
        tashiloader.setVisibility(View.VISIBLE);
        @SuppressLint({"NewApi", "LocalSuppress"}) RequestQueue requestQueue = Volley.newRequestQueue(Objects.requireNonNull(getActivity()));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.API_News, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.optInt("success") == 1) {
                            tashiloader.setVisibility(View.GONE);
                            nodatatvb.setVisibility(View.GONE);
                            newsModelDbArrayList.clear();
                            dbOpenHelper.deteleNewstable();
                            JSONArray news_list = jsonObject.getJSONArray("news_list");
                            for (int i = 0; i < news_list.length(); i++) {
                                NewsModelDb newsModel = new NewsModelDb();
                                JSONObject newsdata = news_list.getJSONObject(i);
                                newsModel.setNews_Id(newsdata.optString("int_glcode"));
                                newsModel.setNews_link(newsdata.optString("link"));
                                newsModel.setNews_title(newsdata.optString("title"));
                                newsModel.setNews_Image(newsdata.optString("image"));
                                newsModel.setLogo(newsdata.optString("logo"));
                                newsModel.setLink_url(newsdata.optString("link_url"));
                                if (IsFavouriteAvailable()) {
                                    if (checkDataForFavourite(newsdata.optString("int_glcode"))) {
                                        newsModel.setIsFavourite("true");
                                    } else {
                                        newsModel.setIsFavourite("false");
                                    }
                                } else {
                                    newsModel.setIsFavourite("false");
                                }
                                dbOpenHelper.addNewsData(newsdata.optString("int_glcode"), newsdata.optString("title"), newsdata.optString("image"), newsdata.optString("logo"), newsdata.optString("link"), newsModel.getIsFavourite(),newsdata.optString("link_url"));
                                newsModelDbArrayList.add(newsModel);
                            }

                            if (newsModelDbArrayList.isEmpty()) {
                                nodatatvb.setVisibility(View.VISIBLE);
                                nodatatvb.setText("No News Found");
                            }

                            CheckFavourite();
//                            String data = gson.toJson(newsModelDbArrayList);
//                            dbOpenHelper.addnewsobject(data);
                            NewsAdapter newsAdapter = new NewsAdapter(newsModelDbArrayList);
                            home_recycler.setAdapter(newsAdapter);

                        } else {
                            tashiloader.setVisibility(View.GONE);
                            nodatatvb.setVisibility(View.VISIBLE);
                            nodatatvb.setText(jsonObject.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        tashiloader.setVisibility(View.GONE);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Please Try After Some Time", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("API", "news_list");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(90000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

    public boolean IsFavouriteAvailable() {
        newsModelFavouriteArrayList = dbOpenHelper.getFavouriteNews();
        if (!newsModelFavouriteArrayList.isEmpty()) {
            return true;
        }
        return false;
    }

    public boolean checkDataForFavourite(String NewsId) {
        for (int i = 0; i < newsModelFavouriteArrayList.size(); i++) {
            if (NewsId.equalsIgnoreCase(newsModelFavouriteArrayList.get(i).getNews_Id())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public void CheckFavourite() {
        String NewsId = SharedPrefs.getSharedPref().getString(SharedPrefs.userdetail.News_Id, "N/A");
        newsModelFavouriteArrayList = dbOpenHelper.getFavouriteNews();
        if (!NewsId.equalsIgnoreCase("N/A")) {
            String[] elements = NewsId.split(",");
            deletedId = Arrays.asList(elements);
            if (IsFavouriteAvailable()) {
                for (int i = 0; i < newsModelFavouriteArrayList.size(); i++) {
                    for (int j = 0; j < deletedId.size(); j++) {
                        if (newsModelFavouriteArrayList.get(i).getNews_Id().equalsIgnoreCase(deletedId.get(j))) {
                            Log.d("TAG", "CheckFavourite:if " + deletedId.get(j) + "+++++" + newsModelFavouriteArrayList.get(i));
                            dbOpenHelper.deleteFavouriteNewsData(deletedId.get(j));
                        }
                    }
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        context.registerReceiver(messageReceiver, new IntentFilter("broadcast_chat_message"));
        Constant.active = true;
    }

    @Override
    public void onPause() {
        super.onPause();
        context.unregisterReceiver(messageReceiver);
        Constant.active = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Constant.active = false;
    }

    public static class MyUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
            if (ex.getClass().equals(OutOfMemoryError.class)) {
                try {
                    android.os.Debug.dumpHprofData("/sdcard/dump.hprof");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            ex.printStackTrace();
        }
    }
}
