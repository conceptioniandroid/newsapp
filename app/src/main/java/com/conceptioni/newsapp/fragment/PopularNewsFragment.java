package com.conceptioni.newsapp.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.TashieLoader;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.conceptioni.newsapp.NewsApp;
import com.conceptioni.newsapp.R;
import com.conceptioni.newsapp.adapter.PopularNewsAdapter;
import com.conceptioni.newsapp.database.DBOpenHelper;
import com.conceptioni.newsapp.model.NewsModelDb;
import com.conceptioni.newsapp.utils.Constant;
import com.conceptioni.newsapp.utils.RecyclerTouchListener;
import com.conceptioni.newsapp.utils.TextViewRegularBold;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class PopularNewsFragment extends Fragment {

    View view;
    RecyclerView home_recycler;
    TashieLoader tashiloader;
    ArrayList<NewsModelDb> newsModelDbArrayList = new ArrayList<>();
    ArrayList<NewsModelDb> newsModelDbArrayList1 = new ArrayList<>();
    ArrayList<NewsModelDb> newsModelFavouriteArrayList = new ArrayList<>();
    List<NewsModelDb> newsModelDbofflineArrayList = new ArrayList<>();
    List<NewsModelDb> newspopularNewsModelDbList = new ArrayList<>();
    DBOpenHelper dbOpenHelper;
    SQLiteDatabase sqLiteDatabase;
    Gson gson;
    TextViewRegularBold nodatatvb;

    public static boolean isNetConnectionAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) NewsApp.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.popular_news_fragment, container, false);
        Thread.currentThread().setDefaultUncaughtExceptionHandler(new MyUncaughtExceptionHandler());
        initview();
        return view;
    }

    private void initview() {
        home_recycler = view.findViewById(R.id.home_recycler);
        tashiloader = view.findViewById(R.id.tashiloader);
        nodatatvb = view.findViewById(R.id.nodatatvb);

        dbOpenHelper = new DBOpenHelper(getActivity());
        sqLiteDatabase = dbOpenHelper.getWritableDatabase();
        sqLiteDatabase = dbOpenHelper.getReadableDatabase();

        gson = new Gson();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        home_recycler.setLayoutManager(layoutManager);

        if (isNetConnectionAvailable()) {
            CallNews();
        } else {
            newspopularNewsModelDbList.clear();
            newspopularNewsModelDbList = dbOpenHelper.getallpopularNews();
            Log.d("TAG", "initview: " + newspopularNewsModelDbList.size());
            if (!newspopularNewsModelDbList.isEmpty()) {
                nodatatvb.setVisibility(View.GONE);
                home_recycler.setVisibility(View.VISIBLE);
                PopularNewsAdapter newsAdapter = new PopularNewsAdapter(newspopularNewsModelDbList);
                home_recycler.setAdapter(newsAdapter);
            } else {
                nodatatvb.setVisibility(View.VISIBLE);
                nodatatvb.setText("No News Found");
            }
//                String NewsData = dbOpenHelper.getpopularNews();
//                if (!NewsData.equalsIgnoreCase("")) {
//                    newsModelDbofflineArrayList.clear();
//                    newsModelDbofflineArrayList = gson.fromJson(NewsData, new TypeToken<List<NewsModelDb>>() {}.getType());
//                    PopularNewsAdapter newsAdapter = new PopularNewsAdapter(newsModelDbofflineArrayList);
//                    home_recycler.setAdapter(newsAdapter);
//                }
        }

        home_recycler.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), home_recycler, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                final ImageView favouriteiv = view.findViewById(R.id.favouriteiv);
                final ImageView shareiv = view.findViewById(R.id.shareiv);
                favouriteiv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isNetConnectionAvailable()) {
                            if (newsModelDbArrayList.get(position).getIsFavourite().equalsIgnoreCase("true")) {
                                favouriteiv.setImageResource(R.drawable.unfil_favorite);
                                newsModelDbArrayList.get(position).setIsFavourite("false");
                                dbOpenHelper.deleteFavouriteNewsData(newsModelDbArrayList.get(position).getNews_Id());
                            } else {
                                favouriteiv.setImageResource(R.drawable.fil_favorite);
                                newsModelDbArrayList.get(position).setIsFavourite("true");
                                dbOpenHelper.addfavouritenews(newsModelDbArrayList.get(position).getNews_Id(), newsModelDbArrayList.get(position).getNews_title(), newsModelDbArrayList.get(position).getNews_Image(), newsModelDbArrayList.get(position).getLogo(), newsModelDbArrayList.get(position).getNews_link(), newsModelDbArrayList.get(position).getIsFavourite(),newsModelDbArrayList.get(position).getLink_url());
                            }
                        } else {
                            List<NewsModelDb> newsModelDbList = new ArrayList<>();
                            newsModelDbList = dbOpenHelper.getallpopularNews();
                            List<NewsModelDb> latestnewsList = new ArrayList<>();
                            latestnewsList = dbOpenHelper.getnews(newspopularNewsModelDbList.get(position).getNews_Id());
                            if (newsModelDbList.get(position).getIsFavourite().equalsIgnoreCase("true")) {
                                if (!newsModelDbList.isEmpty()) {
                                    newsModelDbList.get(position).setIsFavourite("false");
                                    dbOpenHelper.updatepopularnewsdata(newsModelDbList.get(position).getNews_Id(), newsModelDbList.get(position).getNews_title(), newsModelDbList.get(position).getNews_Image(), newsModelDbList.get(position).getLogo(), newsModelDbList.get(position).getNews_link(), newsModelDbList.get(position).getIsFavourite(),newsModelDbList.get(position).getLink_url());
                                }
                                if (!latestnewsList.isEmpty()) {
                                    latestnewsList.get(0).setIsFavourite("false");
                                    dbOpenHelper.updatenewsdata(latestnewsList.get(0).getNews_Id(), latestnewsList.get(0).getNews_title(), latestnewsList.get(0).getNews_Image(), latestnewsList.get(0).getLogo(), latestnewsList.get(0).getNews_link(), latestnewsList.get(0).getIsFavourite(),latestnewsList.get(0).getLink_url());
                                }
                                favouriteiv.setImageResource(R.drawable.unfil_favorite);
                                int deleterow = dbOpenHelper.deleteFavouriteNewsData(newsModelDbList.get(position).getNews_Id());
                                if (deleterow > 0) {
                                    dbOpenHelper.updatepopularnewsdata(newsModelDbList.get(position).getNews_Id(), newsModelDbList.get(position).getNews_title(), newsModelDbList.get(position).getNews_Image(), newsModelDbList.get(position).getLogo(), newsModelDbList.get(position).getNews_link(), newsModelDbList.get(position).getIsFavourite(),newsModelDbList.get(position).getLink_url());
                                }
                            } else {
                                favouriteiv.setImageResource(R.drawable.fil_favorite);
                                if (!newsModelDbList.isEmpty()) {
                                    newsModelDbList.get(position).setIsFavourite("true");
                                    dbOpenHelper.updatepopularnewsdata(newsModelDbList.get(position).getNews_Id(), newsModelDbList.get(position).getNews_title(), newsModelDbList.get(position).getNews_Image(), newsModelDbList.get(position).getLogo(), newsModelDbList.get(position).getNews_link(), newsModelDbList.get(position).getIsFavourite(),newsModelDbList.get(position).getLink_url());
                                }
                                if (!latestnewsList.isEmpty()) {
                                    Log.d("TAG", "onClick: " + latestnewsList.get(0).getNews_Id());
                                    latestnewsList.get(0).setIsFavourite("true");
                                    dbOpenHelper.updatenewsdata(latestnewsList.get(0).getNews_Id(), latestnewsList.get(0).getNews_title(), latestnewsList.get(0).getNews_Image(), latestnewsList.get(0).getLogo(), latestnewsList.get(0).getNews_link(), latestnewsList.get(0).getIsFavourite(),latestnewsList.get(0).getLink_url());
                                }

                                dbOpenHelper.addfavouritenews(newsModelDbList.get(position).getNews_Id(), newsModelDbList.get(position).getNews_title(), newsModelDbList.get(position).getNews_Image(), newsModelDbList.get(position).getLogo(), newsModelDbList.get(position).getNews_link(), newsModelDbList.get(position).getIsFavourite(),newsModelDbList.get(position).getLink_url());

                            }
                        }
                    }
                });

                shareiv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                        String sAux = "\nLet me recommend you this News\n\n";
                        if (isNetConnectionAvailable()) {
//                        sAux = sAux + "https://play.google.com/store/movies/details/Jurassic_World_Fallen_Kingdom?id=OexCdfICdGU \n\n";
                            sAux = sAux + newsModelDbArrayList.get(position).getNews_link();
                            i.putExtra(Intent.EXTRA_TEXT, sAux);
                            startActivity(Intent.createChooser(i, "choose one"));
                        } else {
                            sAux = sAux + newspopularNewsModelDbList.get(position).getNews_link();
                            i.putExtra(Intent.EXTRA_TEXT, sAux);
                            startActivity(Intent.createChooser(i, "choose one"));
                        }
                    }
                });
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void CallNews() {
        tashiloader.setVisibility(View.VISIBLE);
        @SuppressLint({"NewApi", "LocalSuppress"}) RequestQueue requestQueue = Volley.newRequestQueue(Objects.requireNonNull(getActivity()));
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.API_News_Recent, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.optInt("success") == 1) {
                            tashiloader.setVisibility(View.GONE);
                            newsModelDbArrayList.clear();
                            dbOpenHelper.deletepopularenews();
                            JSONArray news_list = jsonObject.getJSONArray("News_Recent");
                            for (int i = 0; i < news_list.length(); i++) {
                                NewsModelDb newsModel = new NewsModelDb();
                                JSONObject newsdata = news_list.getJSONObject(i);
                                newsModel.setNews_Id(newsdata.optString("int_glcode"));
                                newsModel.setNews_link(newsdata.optString("link"));
                                newsModel.setNews_title(newsdata.optString("title"));
                                newsModel.setNews_Image(newsdata.optString("image"));
                                newsModel.setLogo(newsdata.optString("logo"));
                                newsModel.setLink_url(newsdata.optString("link_url"));
                                if (IsFavouriteAvailable()) {
                                    if (checkDataForFavourite(newsdata.optString("int_glcode"))) {
                                        newsModel.setIsFavourite("true");
                                    } else {
                                        newsModel.setIsFavourite("false");
                                    }
                                } else {
                                    newsModel.setIsFavourite("false");
                                }
                                dbOpenHelper.Addpopularnews(newsdata.optString("int_glcode"), newsdata.optString("title"), newsdata.optString("image"), newsdata.optString("logo"), newsdata.optString("link"), newsModel.getIsFavourite(),newsdata.optString("link_url"));
                                newsModelDbArrayList.add(newsModel);
                            }
//                            String data = gson.toJson(newsModelDbArrayList);
//                            dbOpenHelper.addpopularnewsobject(data);
                            PopularNewsAdapter newsAdapter = new PopularNewsAdapter(newsModelDbArrayList);
                            home_recycler.setAdapter(newsAdapter);

                        } else {
                            tashiloader.setVisibility(View.GONE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        tashiloader.setVisibility(View.GONE);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(), "Please Try After Some Time", Toast.LENGTH_LONG).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("API", "news_recent");
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(90000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

    public boolean IsFavouriteAvailable() {
        newsModelFavouriteArrayList = dbOpenHelper.getFavouriteNews();
        if (!newsModelFavouriteArrayList.isEmpty()) {
            return true;
        }
        return false;
    }

    public boolean checkDataForFavourite(String NewsId) {
        Log.d("TAG", "checkDataForFavourite: " + newsModelFavouriteArrayList.size());
        for (int i = 0; i < newsModelFavouriteArrayList.size(); i++) {
            if (NewsId.equalsIgnoreCase(newsModelFavouriteArrayList.get(i).getNews_Id())) {
                return true;
            }
        }
        return false;
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public static class MyUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
            if (ex.getClass().equals(OutOfMemoryError.class)) {
                try {
                    android.os.Debug.dumpHprofData("/sdcard/dump.hprof");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            ex.printStackTrace();
        }
    }
}
