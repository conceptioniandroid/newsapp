package com.conceptioni.newsapp.model;

public class NewsModelDb {


    private String News_Id;
    private String News_title;
    private String News_link;
    private String logo;
    private String IsFavourite;
    private String link_url;

    public String getLink_url() {
        return link_url;
    }

    public void setLink_url(String link_url) {
        this.link_url = link_url;
    }

    public String getIsFavourite() {
        return IsFavourite;
    }

    public void setIsFavourite(String isFavourite) {
        IsFavourite = isFavourite;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getNews_Id() {
        return News_Id;
    }

    public void setNews_Id(String news_Id) {
        News_Id = news_Id;
    }

    public String getNews_title() {
        return News_title;
    }

    public void setNews_title(String news_title) {
        News_title = news_title;
    }

    public String getNews_link() {
        return News_link;
    }

    public void setNews_link(String news_link) {
        News_link = news_link;
    }

    public String getNews_Image() {
        return News_Image;
    }

    public void setNews_Image(String news_Image) {
        News_Image = news_Image;
    }

    private String News_Image;
}
