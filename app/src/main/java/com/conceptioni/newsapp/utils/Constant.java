package com.conceptioni.newsapp.utils;

import java.util.ArrayList;
import java.util.List;

public class Constant {

    public static String API_BASE_URL = "http://citechnology.in/news/api/";
    public static String API_News = API_BASE_URL + "news_list";
    public static String API_News_Recent = API_BASE_URL + "NewsRecent";
    public static String Api_userNotification = API_BASE_URL + "userNotification";


    public static boolean active;
    public static String IsFrom = "";

    public static List<String> deletedId = new ArrayList<>();
}
