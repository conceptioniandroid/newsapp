package com.conceptioni.newsapp.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.conceptioni.newsapp.NewsApp;

public class SharedPrefs {

    public static SharedPreferences getSharedPref() {
        String sharedPrefenceName = "NewsApp";
        return NewsApp.getContext().getSharedPreferences(sharedPrefenceName, Context.MODE_PRIVATE);
    }

    public interface userdetail {
        String user_id = "user_id";
        String country_code = "country_code";
        String News_Id = "News_Id";
    }

    public interface tokendetail {
        String refreshtoken = "refreshtoken";
    }

}
