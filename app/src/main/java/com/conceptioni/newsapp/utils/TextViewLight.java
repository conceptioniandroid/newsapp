package com.conceptioni.newsapp.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by 123 on 21-02-2017.
 */

public class TextViewLight extends TextView {
    public TextViewLight(Context context) {
        super(context);
        init();
    }

    public TextViewLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewLight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/QUICKSAND-LIGHT.TTF");
        setTypeface(tf);
    }
}
