package com.conceptioni.newsapp.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by admin on 5/6/2017.
 */

public class TextViewRegularBold extends TextView {
    public TextViewRegularBold(Context context) {
        super(context);
        init();
    }

    public TextViewRegularBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextViewRegularBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/QUICKSAND-BOLD.TTF");
        setTypeface(tf);
    }
}
