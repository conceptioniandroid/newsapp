package com.conceptioni.newsapp.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by admin on 10/9/2017.
 */

public class TextviewSemiBold extends TextView {

    public TextviewSemiBold(Context context) {
        super(context);
        init();
    }

    public TextviewSemiBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextviewSemiBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/QUICKSAND-MEDIUM.TTF");
        setTypeface(tf);
    }
}
